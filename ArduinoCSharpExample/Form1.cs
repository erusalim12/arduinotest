﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ArduinoCSharpExample.Properties;

namespace ArduinoCSharpExample
{
    public partial class Form1 : Form
    {
        private bool Lighter { get; set; } = false;
        private SerialPort SerialPort1 { get; set; }
        public Form1()
        {
            InitializeComponent();

            SerialPort1 = new SerialPort();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            ChangeImage(Lighter);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ChangeImage(Lighter);
            Lighter = !Lighter;
        }

        private void ChangeImage( bool state)
        {
            if (state)
            {
                pictureBox1.Image = Resources._1;
                SendToPort("Com13");
            }
            else
            {
                pictureBox1.Image = Resources._0;
                SendToPort("Com13");
            }
        
        }

        private void SendToPort(string portName)
        {
            try
            {
                // настройки порта
                SerialPort1.PortName = portName;
                SerialPort1.BaudRate = 256000;
                SerialPort1.DataBits = 8;
                SerialPort1.Parity = System.IO.Ports.Parity.None;
                SerialPort1.StopBits = System.IO.Ports.StopBits.One;
                SerialPort1.ReadTimeout = 1000;
                SerialPort1.WriteTimeout = 1000;
                SerialPort1.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: невозможно открыть порт:" + e.ToString());
                return;
            }
        }


    }
}
